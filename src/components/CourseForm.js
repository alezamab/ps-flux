import React from "react";
import TextInput from "./common/TextInput";
import PropTypes from "prop-types";

const CourseForm = ({course, onChange, onSubmit, errors}) => {
    return (
        <form onSubmit={onSubmit}>
            <div className={"form-group"}>
                <TextInput
                    id={"title"}
                    label={"Title"}
                    onChange={onChange}
                    name={"title"}
                    value={course.title}
                    error={errors.title}
                />
            </div>
            <div className={"form-group"}>
                <label htmlFor={"author"}>Author</label>
                <div className={"field"}>
                    <select
                        id={"author"}
                        name={"authorId"}
                        onChange={onChange}
                        value={course.authorId || ""}
                        className={"form-control"}>
                        <option value=""></option>
                        <option value="1">Cory House</option>
                        <option value="2">Scott Allen</option>
                    </select>
                </div>
                {errors.authorId && (
                    <div className={"alert alert-danger"}>{errors.authorId}</div>
                )}
            </div>

            <TextInput
                id={"category"}
                label={"Category"}
                name={"category"}
                onChange={onChange}
                value={course.category}
                error={errors.category}
            />

            <input type={"submit"} value={"save"} className={"btn btn-primary"}/>

        </form>
    );
}

CourseForm.propTypes = {
    course: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired
};

export default CourseForm;